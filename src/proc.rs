use serde::Deserialize;
use serde::Serialize;
use std::collections::HashMap;
use std::fmt::Write;
use std::path::Path;
use std::path::PathBuf;
use toml::Table;

use crate::endpoint;
use crate::inputoutput::BindingOps;
use crate::inputoutput::InputOutput;
use crate::GlobalState;

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
#[serde(deny_unknown_fields)]
pub struct SubTask {
    pub proc: String,
    #[serde(flatten)]
    pub io: InputOutput,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "UPPERCASE")]
pub enum Method {
    Get,
    Post,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
#[serde(deny_unknown_fields)]
pub struct Endpoint {
    pub method: Method,
    pub path: String,
    #[serde(default = "return_true")]
    pub auth: bool,
    #[serde(flatten)]
    pub io: InputOutput,
    pub args: Option<Vec<String>>,
    pub cache_file: Option<String>,
}

pub fn return_true() -> bool {
    true
}

pub fn argument_replace(mut source: String, args: &[&str], inputs: &mut Table) -> String {
    for arg in args {
        let value = inputs
            .get(*arg)
            .unwrap()
            .as_str()
            .expect("Argument isn't string");
        let match_str = format!("{{{}}}", arg);
        source = source.replace(&match_str, value);
        inputs.remove(*arg);
    }
    source
}

impl Endpoint {
    pub async fn call(
        &self,
        global: &mut GlobalState,
        raw_inputs: Table,
    ) -> Result<Table, reqwest::Error> {
        let mut inputs = raw_inputs.to_owned();
        let mut url = endpoint(&self.path);
        if let Some(args) = self.args.as_ref() {
            let args: Vec<&str> = args.into_iter().map(|s| &s[..]).collect();
            url = argument_replace(url, args.as_slice(), &mut inputs);
        }

        let client = &mut global.client;

        let mut req = match self.method {
            Method::Get => client.get(url),
            Method::Post => client.post(url),
        };
        if inputs.len() > 0 {
            let body = serde_json::to_string(&inputs).expect("Failed to serialize inputs");
            // println!("Body is: {body:#}");
            req = req.header("Content-Type", "application/json").body(body);
        }

        if self.auth == true {
            let auth_token = global.values["agent"]["token"].as_str().unwrap();
            req = req.header("Authorization", format!("Bearer {}", auth_token))
        }

        let res = req.try_clone().unwrap().send().await?;
        if !res.status().is_success() {
            println!("{:#?}", req);
            println!("{:#?}", res);
        }
        if let Some(content_type) = res.headers().get("Content-Type") {
            let content_type = content_type.to_str().unwrap();
            if content_type.starts_with("application/json") {
                let json = res.json().await?;
                println!("TOML response:\n{:#}", json);
                if let Some(cache_file) = self.cache_file.as_ref() {
                    let mut cache_file = cache_file.to_owned();
                    if let Some(args) = self.args.as_ref() {
                        let args: Vec<&str> = args.into_iter().map(|s| &s[..]).collect();
                        cache_file = argument_replace(
                            cache_file,
                            args.as_slice(),
                            &mut raw_inputs.to_owned(),
                        );
                    }
                    write!(cache_file, ".toml").unwrap();
                    let path = Path::new(&cache_file);
                    std::fs::create_dir_all(path.parent().unwrap()).unwrap();
                    std::fs::write(path, toml::to_string(&json).unwrap()).unwrap();
                }
                return Ok(json);
            }
        }
        let text = res.text().await?;
        println!("Text resoponse:  {}", text);
        Ok(Table::new())
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
#[serde(deny_unknown_fields)]
pub struct Proc {
    pub steps: Option<Vec<SubTask>>,
    pub endpoint: Option<Endpoint>,
}

impl Proc {
    pub async fn perform_step(
        &self,
        global: &mut GlobalState,
        args: &Table,
        state: &mut Table,
        task: &SubTask,
    ) -> Result<(), String> {
        let inputs = task.io.input.into_input_table(global, state, args)?;
        todo!()
    }

    pub async fn perform_steps(
        &self,
        global: &mut GlobalState,
        args: &Table,
        state: &mut Table,
    ) -> Result<(), String> {
        let Some(steps) = &self.steps else {
            // Nothing to do.
            return Ok(());
        };
        for task in steps.iter() {
            self.perform_step(global, args, state, task).await?;
        }
        Ok(())
    }

    pub async fn perform_endpoint(
        &self,
        global: &mut GlobalState,
        args: &Table,
        state: &mut Table,
    ) -> Result<(), String> {
        let Some(endpoint) = &self.endpoint else {
            // Nothing to do.
            return Ok(());
        };
        let inputs = endpoint.io.input.into_input_table(global, state, args)?;
        if let Some(expected_args) = endpoint.args.as_ref() {
            for expected in expected_args {
                if !inputs.contains_key(expected) {
                    return Err(format!("Missing argument '{}'", expected));
                }
            }
        }
        let mut raw_output = endpoint
            .call(global, inputs)
            .await
            .map_err(|e| format!("Endpoint errored: {}", e))?;
        endpoint
            .io
            .output
            .capture_output(global, state, &mut raw_output)?;
        Ok(())
    }

    pub async fn perform(&self, global: &mut GlobalState, args: &Table) -> Result<(), String> {
        let mut state = Table::new();
        let state = &mut state;
        self.perform_steps(global, args, state).await?;
        self.perform_endpoint(global, args, state).await?;
        Ok(())
    }
}

pub type ProcCategory = HashMap<String, Proc>;

pub type ProcRoot = HashMap<String, ProcCategory>;

pub async fn read_procs(root_dir: PathBuf) -> Result<ProcRoot, std::io::Error> {
    assert!(root_dir.is_dir());

    let category_paths = root_dir.read_dir()?;
    let category_paths = category_paths.filter_map(|entry| -> Option<_> {
        let entry = entry.ok()?;
        let dir_name = entry.file_name().into_string().ok()?;
        let dir_content = entry.path().read_dir().ok()?;
        Some((dir_name, dir_content))
    });

    let mut proc_root = ProcRoot::new();
    for (category_name, proc_paths) in category_paths {
        let mut category = ProcCategory::new();

        for def_path in proc_paths.filter_map(Result::ok) {
            let file_content = std::fs::read_to_string(def_path.path())?;

            let procs = toml::from_str::<HashMap<String, Proc>>(&file_content)
                .expect("Failed to deserialize proc");
            category.extend(procs);
        }

        proc_root.insert(category_name, category);
    }

    return Ok(proc_root);
}
