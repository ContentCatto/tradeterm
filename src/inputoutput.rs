use super::identifier::Identifier;
use super::GlobalState;
use serde::{Deserialize, Serialize};
use toml::Table;

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
#[serde(deny_unknown_fields)]
pub struct Binding {
    pub from: Identifier,
    pub to: Identifier,
}

impl Binding {
    pub fn new(from: &str, to: &str) -> Self {
        Self {
            from: Identifier::new(from.to_owned()),
            to: Identifier::new(to.to_owned()),
        }
    }

    pub fn transfer(&self, from_data: &Table, to_data: &mut Table) -> Result<(), String> {
        println!("Transfer '{}' to '{}'", self.from, self.to);
        // println!("From: {:#?}", from_data);
        self.transfer_impl(from_data, to_data)
    }

    fn transfer_impl(&self, from_data: &Table, to_data: &mut Table) -> Result<(), String> {
        assert_ne!(self.to.parts.len(), 0);
        let key = &self.to.parts[0];
        if self.to.parts.len() == 1 {
            let mut val = &toml::Value::Table(from_data.to_owned());
            for key in self.from.parts.iter() {
                val = val.get(key).ok_or(format!("KeyError: {}", key))?;
            }

            let val = val.to_owned();
            to_data.insert(key.to_owned(), val);
            return Ok(());
        }
        if !to_data.contains_key(key) {
            let val = toml::Value::Table(Table::new());
            to_data.insert(key.to_owned(), val);
        }
        let next = to_data.get_mut(key).unwrap();
        let to_data: &mut Table = match next {
            toml::Value::Table(t) => t,
            _ => return Err(format!("NotATableError: {}", next)),
        };
        let from = self.from.to_owned();
        let to = Identifier::from_parts(Vec::from(&self.to.parts[1..]));
        let recursive_binding = Self { from, to };
        recursive_binding.transfer_impl(from_data, to_data)
    }

    fn pop_from_prefix(&self, prefix: &'static str) -> Option<Self> {
        if let Some(ident) = self.from.pop_prefix(prefix) {
            Some(Self {
                from: ident,
                to: self.to.to_owned(),
            })
        } else {
            None
        }
    }

    fn pop_to_prefix(&self, prefix: &'static str) -> Option<Self> {
        if let Some(ident) = self.to.pop_prefix(prefix) {
            Some(Self {
                from: self.from.to_owned(),
                to: ident,
            })
        } else {
            None
        }
    }
}

pub type Bindings = Vec<Binding>;

pub trait BindingOps {
    fn into_input_table(
        &self,
        global: &GlobalState,
        state: &Table,
        args: &Table,
    ) -> Result<Table, String>;

    fn capture_output(
        &self,
        global: &mut GlobalState,
        state: &mut Table,
        raw_output: &Table,
    ) -> Result<(), String>;
}

impl BindingOps for Bindings {
    fn into_input_table(
        &self,
        global: &GlobalState,
        state: &Table,
        args: &Table,
    ) -> Result<Table, String> {
        let mut input = Table::with_capacity(self.len());
        for binding in self.iter() {
            if let Some(binding) = binding.pop_from_prefix("global") {
                binding.transfer(&global.values, &mut input)?;
            } else if let Some(binding) = binding.pop_from_prefix("args") {
                binding.transfer(args, &mut input)?;
            } else {
                binding.transfer(state, &mut input)?;
            }
        }
        Ok(input)
    }

    fn capture_output(
        &self,
        global: &mut GlobalState,
        state: &mut Table,
        raw_output: &Table,
    ) -> Result<(), String> {
        for binding in self.iter() {
            if let Some(binding) = binding.pop_to_prefix("global") {
                binding.transfer(raw_output, &mut global.values)?;
            } else {
                binding.transfer(raw_output, state)?;
            }
        }
        Ok(())
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct InputOutput {
    pub input: Bindings,
    pub output: Bindings,
}
