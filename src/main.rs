#![feature(half_open_range_patterns_in_slices)]

use std::{
    io::{stdin, stdout, Write},
    path::{Path, PathBuf},
};

use reqwest::Client;
use toml::Table;

mod identifier;
mod inputoutput;
mod proc;

use inputoutput::Binding;
use proc::{Proc, ProcRoot};

pub struct GlobalState {
    client: Client,
    values: Table,
}

const STATE_PATH: &str = "secrets/state.toml";
static mut PROC_LOOKUP: Option<ProcRoot> = None;

pub fn get_procs() -> &'static ProcRoot {
    unsafe { &PROC_LOOKUP.as_ref().unwrap() }
}

pub fn get_proc(path: &str) -> Result<&Proc, String> {
    let parts: Vec<_> = path.split('.').collect();
    if parts.len() != 2 {
        return Err(format!("Invalid proc path: {}", path));
    }

    let category = parts[0];
    let name = parts[1];
    unsafe {
        let proc = PROC_LOOKUP
            .as_ref()
            .unwrap()
            .get(category)
            .and_then(|cat| cat.get(name));
        proc.ok_or_else(|| format!("KeyError: {:?}", path))
    }
}

#[tokio::main]
async fn main() -> Result<(), reqwest::Error> {
    let mut global = GlobalState {
        client: Client::new(),
        values: toml::from_str(&std::fs::read_to_string(Path::new(STATE_PATH)).unwrap())
            .expect("Failed to deserialize state"),
    };
    println!("Loaded state values:");
    println!("{}", toml::to_string(&global.values).unwrap());
    println!();

    unsafe {
        PROC_LOOKUP = Some(
            proc::read_procs(PathBuf::from("data/define"))
                .await
                .unwrap(),
        );
    };
    println!("Loaded {} defines from dir.", get_procs().len());
    println!("Defines:");
    println!("{:#?}", get_procs());
    println!("{}", toml::to_string(get_procs()).unwrap());

    loop {
        let mut command_str = String::new();
        print!("> ");
        stdout().lock().flush().unwrap();
        stdin()
            .read_line(&mut command_str)
            .expect("Failed to read stdin");
        command_str.remove(command_str.len() - 1);
        let command: Vec<_> = command_str.split_whitespace().collect();
        if command.len() < 1 {
            continue;
        }

        async fn run_proc(proc_path: &str, args: &Table, global: &mut GlobalState) {
            let proc = get_proc(proc_path);
            match proc {
                Ok(proc) => proc.perform(global, args).await.unwrap(),
                Err(err) => {
                    println!("{}", err);
                }
            }
        }
        fn parse_args(args: &[&str]) -> Table {
            for arg in args.iter() {
                if arg.find('=') != arg.rfind('=') {
                    panic!("More than 1 equals in argument.");
                }
            }
            let combined = args.join("\n");
            toml::from_str(&combined).unwrap()
            // let args: Vec<(&str, &str)> = args
            //     .into_iter()
            //     .map(|s| {
            //         let eq_index = s.find('=').unwrap();
            //         (&s[..eq_index], &s[eq_index + 1..])
            //     })
            //     .collect();

            // let mut result = Table::new();

            // for (ident, value) in args {
            //     let binding = Binding::new("value", ident);
            //     let value: toml::Value = toml::from_str(value).unwrap();
            //     let mut source = Table::new();
            //     source.insert("value".to_owned(), value);
            //     binding.transfer(&source, &mut result).unwrap();
            // }
            // result
        }

        match &command[..] {
            &["exit"] => break,

            &["proc"] => println!("Please supply a proc path to run:  PROC <proc path>"),
            &["proc", proc_path, ..] => {
                run_proc(proc_path, &parse_args(&command[2..]), &mut global).await
            }

            // Aliases
            &["deet"] => run_proc("agent.details", &Table::new(), &mut global).await,
            &["wp", ..] => run_proc("nav.waypoint", &parse_args(&command[1..]), &mut global).await,

            _ => println!("Unknown command {:?}", command_str),
        }

        std::fs::write(
            Path::new(STATE_PATH),
            toml::to_string_pretty(&global.values).unwrap(),
        )
        .unwrap();
    }
    Ok(())
}

fn endpoint(s: &str) -> String {
    assert!(s.starts_with('/'));
    format!("https://api.spacetraders.io/v2{}", s)
}
