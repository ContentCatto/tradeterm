use serde;
use serde::de::Visitor;
use serde::Deserialize;
use serde::Serialize;
use std;
use std::fmt::Display;

#[derive(Debug, Clone)]
pub struct Identifier {
    pub s: String,
    pub parts: Vec<String>,
}

impl Identifier {
    pub fn new(s: String) -> Self {
        let parts = IdentifierVisitor::from_str(&s);
        Self { s, parts }
    }

    pub fn from_parts(parts: Vec<String>) -> Self {
        let s = parts.join(".");
        assert_eq!(s.split('.').count(), parts.len());
        Self { s, parts }
    }

    pub fn from_slice(parts: &[String]) -> Self {
        let parts = Vec::from(parts);
        Identifier::from_parts(parts)
    }

    pub fn pop_prefix(&self, prefix: &'static str) -> Option<Identifier> {
        if self.parts.len() < 2 || self.parts[0] != prefix {
            return None;
        }
        let parts_without_prefix = &self.parts[1..];
        let without_prefix = Identifier::from_slice(parts_without_prefix);
        Some(without_prefix)
    }
}

impl Display for Identifier {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.s)
    }
}

struct IdentifierVisitor;

impl IdentifierVisitor {
    fn from_str(s: &str) -> Vec<String> {
        assert_eq!(s.contains(char::is_whitespace), false);
        s.split('.').map(str::to_owned).collect()
    }
}

impl<'de> Visitor<'de> for IdentifierVisitor {
    type Value = Vec<String>;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        formatter.write_str("a string composed of period delimited path components")
    }

    fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        Ok(Self::from_str(v))
    }

    fn visit_string<E>(self, v: String) -> Result<Self::Value, E>
    where
        E: serde::de::Error,
    {
        Ok(Self::from_str(&v))
    }
}

impl<'de> Deserialize<'de> for Identifier {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        deserializer
            .deserialize_str(IdentifierVisitor)
            .map(Identifier::from_parts)
    }
}

impl Serialize for Identifier {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(&self.parts.join("."))
    }
}
